# README.md

Languages: [English](README.md), [French](README.fr-FR.md)\
[(Check version depending of the language)](VERSION.md)\
[(Check changelog of the project)](CHANGELOG.md)

[Taiga of this project](https://tree.taiga.io/project/j-ramsey-readme/)

## Summary

* [Motivation](#motivation)
* [Functionnalities](#functionnalities)
* [Requires](#requires)
* [Installation](#installation)
* [Contributors](#contributors)
* [License](#license)
* [Thanks](#thanks)

## Motivation

As I like to have a correct readme in my projects, I decided to make a repo to make a sample following my vision of how to make a proper README.md.

As I currently consider English as the standard language in programming society, the default language impemented in my default README.md will be English until North Koreans take over the world (just kidding, will be Chinese).

All parts of this readme have a purpose (this section, for example, is a résumé of your project), it's all yours to modify the files and lines to fill your needs.

[Return to summary](#summary)
## Functionnalities

### What's working :
* English in README.md

### Working on :
* French Translation in README.fr-FR.md

### Will be added next :
* Nothing for the moment

### May be added in the future :
* Other languages
	* Chinese
	* Another example
	* ...

[Return to summary](#summary)
## Requires

* A Markdown reader (originally developed for GitHub, may not work as intended for all MD readers)

[Return to summary](#summary)
## Installation

* Clone the project or download and extract the zip file.
* Use and modify the READMEs you need

[Return to summary](#summary)
## Contributors

* Main contributors
	* [J-Ramsey]

* Important contributors
	* [J-Ramsey]

* Other contributors
	* [J-Ramsey] 

[Return to summary](#summary)
## License

This project is under [MIT](LICENSE) License.

[Return to summary](#summary)
## Thanks

* Thanks to all the contributors
* Thanks to [J-Ramsey](https://github.com/J-Ramsey) for his [README.md sample](https://github.com/J-Ramsey/README.md)
* Thanks to [this issue](https://github.com/github/markup/issues/899), especially [kesarion](https://github.com/github/markup/issues/899#issuecomment-259518791) for giving a correct idea for a multilanguage solution (as GitHub doesn't support it for the moment)
* Thanks to [markdown-it](https://github.com/markdown-it/markdown-it) team for [their tool](https://markdown-it.github.io) that let me test my Markdown code

[Return to summary](#summary)

[J-Ramsey]: <https://github.com/J-Ramsey>