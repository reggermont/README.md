# Version : 1.0.1

| Lang | Local | Up-to-date | Description |
| :----: | :-----: | :----------: | ----------- |
| English | en-US  | ✔ | Original version, always up-to-date |
| France | fr-FR  | 1.0.0 | Official description by me |