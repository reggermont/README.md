# README.md

Langues : [Anglais](README.md), [Français](README.fr-FR.md)\
[(Vérifiez la version du readme selon la langue)](VERSION.md)

[Taiga de ce projet](https://tree.taiga.io/project/j-ramsey-readme/)

## Sommaire

* [Motivation](#motivation)
* [Fonctionnalités](#fonctionnalités)
* [Pré-requis](#pré-requis)
* [Installation](#installation)
* [Contributeurs](#contributeurs)
* [Licence](#licence)
* [Remerciements](#remerciements)

## Motivation

Puisque je préfère avoir un readme propre pour mes projets, j'ai décidé de créer un repo afin de réaliser un exemple suivant ma vision d'un bon readme.

Considérant l'Anglais comme la langue principale dans le monde de l'informatique, la langue par défaut dans le README.md sera l'Anglais jusqu'à la domination du monde par les Coréens du Nord (je plaisante, ce sera les Chinois).

Toutes les parties de ce readme ont un but (cette section, par exemple, est un résumé du projet), cela tient à vous de modifier les fichiers et lignes selon votre projet et vos besoins

[Retour au Sommaire](#sommaire)
## Fonctionnalités

### Ce qui fonctionne :
* Version anglaise : README.md

### Travaille actuellement sur :
* Traduction française : README.fr-FR.md

### Sera ajouté par la suite :
* Rien de prévu pour le moment

### Sera éventuellement ajouté plus tard :
* D'autres langues :
	* Chinois,
	* Autre exemple,
	* ...

[Retour au Sommaire](#sommaire)
## Pré-requis

* Un lecteur de fichiers .md (Markdown. Ce fichier est originellement écrit pour un affichage sur GitHub, le résultat ne sera peut-être pas le même pour tous les lecteurs de MD)

[Retour au Sommaire](#sommaire)
## Installation

* Clonez le projet ou téléchargez et extrayez l'archive
* Utilisez et modifiez les README dont vous avez besoin

[Retour au Sommaire](#sommaire)
## Contributeurs

* Contributeurs principaux
	* [J-Ramsey]

* Contributeurs importants
	* [J-Ramsey]

* Autres contributeurs
	* [J-Ramsey] 

[Retour au Sommaire](#sommaire)
## Licence

Ce projet est protégé sous la licence [MIT](LICENSE).

[Retour au Sommaire](#sommaire)
## Thanks

* Merci à tous les contributeurs,
* Merci à [J-Ramsey] pour son exemple de [README.md](https://github.com/J-Ramsey/README.md),
* Merci à [cette issue](https://github.com/github/markup/issues/899), spécialement [kesarion](https://github.com/github/markup/issues/899#issuecomment-259518791) d'avoir partagé une bonne idée de gestion de multilangue (puisque GitHub ne le supporte pas pour le moment),
* Merci à l'équipe de [markdown-it](https://github.com/markdown-it/markdown-it) pour [leur outil](https://markdown-it.github.io) m'ayant permis de tester mon code Markdown.

[Retour au Sommaire](#sommaire)

[J-Ramsey]: <https://github.com/J-Ramsey>