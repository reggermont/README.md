# Changelog

## Summary
* [1.0.1 (2017-11-12)](#101-2017-11-12)
* [1.0.0 (2017-10-27)](#100-2017-10-27)

[Return to summary](#summary)
## 1.0.1 (2017-11-12)

* **New Feature**: 
	* Official french translation
	
* **New Content**: 
	* Adding link to changelog

[Return to summary](#summary)
## 1.0.0 (2017-10-27)

* **Initial release**: Initial release

[Return to summary](#summary)